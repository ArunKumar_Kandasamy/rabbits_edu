# == Schema Information
#
# Table name: dynamic_contents
#
#  id                   :integer          not null, primary key
#  title                :string
#  description          :string
#  slug                 :string
#  status               :boolean
#  notification_type_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

require 'test_helper'

class DynamicContentsControllerTest < ActionController::TestCase
  setup do
    @dynamic_content = dynamic_contents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dynamic_contents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dynamic_content" do
    assert_difference('DynamicContent.count') do
      post :create, dynamic_content: { description: @dynamic_content.description, notification_type_id: @dynamic_content.notification_type_id, slug: @dynamic_content.slug, status: @dynamic_content.status, title: @dynamic_content.title }
    end

    assert_redirected_to dynamic_content_path(assigns(:dynamic_content))
  end

  test "should show dynamic_content" do
    get :show, id: @dynamic_content
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dynamic_content
    assert_response :success
  end

  test "should update dynamic_content" do
    patch :update, id: @dynamic_content, dynamic_content: { description: @dynamic_content.description, notification_type_id: @dynamic_content.notification_type_id, slug: @dynamic_content.slug, status: @dynamic_content.status, title: @dynamic_content.title }
    assert_redirected_to dynamic_content_path(assigns(:dynamic_content))
  end

  test "should destroy dynamic_content" do
    assert_difference('DynamicContent.count', -1) do
      delete :destroy, id: @dynamic_content
    end

    assert_redirected_to dynamic_contents_path
  end
end
