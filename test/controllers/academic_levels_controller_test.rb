# == Schema Information
#
# Table name: academic_levels
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class AcademicLevelsControllerTest < ActionController::TestCase
  setup do
    @academic_level = academic_levels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:academic_levels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create academic_level" do
    assert_difference('AcademicLevel.count') do
      post :create, academic_level: { name: @academic_level.name }
    end

    assert_redirected_to academic_level_path(assigns(:academic_level))
  end

  test "should show academic_level" do
    get :show, id: @academic_level
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @academic_level
    assert_response :success
  end

  test "should update academic_level" do
    patch :update, id: @academic_level, academic_level: { name: @academic_level.name }
    assert_redirected_to academic_level_path(assigns(:academic_level))
  end

  test "should destroy academic_level" do
    assert_difference('AcademicLevel.count', -1) do
      delete :destroy, id: @academic_level
    end

    assert_redirected_to academic_levels_path
  end
end
