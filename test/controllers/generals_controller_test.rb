# == Schema Information
#
# Table name: generals
#
#  id                   :integer          not null, primary key
#  logo_file_name       :string
#  logo_content_type    :string
#  logo_file_size       :integer
#  logo_updated_at      :datetime
#  favicon_file_name    :string
#  favicon_content_type :string
#  favicon_file_size    :integer
#  favicon_updated_at   :datetime
#  page_title           :string
#  meta_description     :text
#  analytics_text       :text
#  phone_number         :string
#  fb_link              :string
#  twitter_link         :string
#  address              :text
#  email                :string
#  home_page_content    :text
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

require 'test_helper'

class GeneralsControllerTest < ActionController::TestCase
  setup do
    @general = generals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:generals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create general" do
    assert_difference('General.count') do
      post :create, general: { address: @general.address, analytics_text: @general.analytics_text, email: @general.email, favicon: @general.favicon, fb_link: @general.fb_link, logo: @general.logo, meta_description: @general.meta_description, page_title: @general.page_title, phone_number: @general.phone_number, twitter_link: @general.twitter_link }
    end

    assert_redirected_to general_path(assigns(:general))
  end

  test "should show general" do
    get :show, id: @general
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @general
    assert_response :success
  end

  test "should update general" do
    patch :update, id: @general, general: { address: @general.address, analytics_text: @general.analytics_text, email: @general.email, favicon: @general.favicon, fb_link: @general.fb_link, logo: @general.logo, meta_description: @general.meta_description, page_title: @general.page_title, phone_number: @general.phone_number, twitter_link: @general.twitter_link }
    assert_redirected_to general_path(assigns(:general))
  end

  test "should destroy general" do
    assert_difference('General.count', -1) do
      delete :destroy, id: @general
    end

    assert_redirected_to generals_path
  end
end
