require 'test_helper'

class AdmissionsControllerTest < ActionController::TestCase
  setup do
    @admission = admissions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admissions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admission" do
    assert_difference('Admission.count') do
      post :create, admission: { address: @admission.address, annual_income: @admission.annual_income, class: @admission.class, dob: @admission.dob, email: @admission.email, father_designation: @admission.father_designation, father_name: @admission.father_name, gender: @admission.gender, mobile: @admission.mobile, mother_designation: @admission.mother_designation, mother_name: @admission.mother_name, name: @admission.name, school: @admission.school }
    end

    assert_redirected_to admission_path(assigns(:admission))
  end

  test "should show admission" do
    get :show, id: @admission
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admission
    assert_response :success
  end

  test "should update admission" do
    patch :update, id: @admission, admission: { address: @admission.address, annual_income: @admission.annual_income, class: @admission.class, dob: @admission.dob, email: @admission.email, father_designation: @admission.father_designation, father_name: @admission.father_name, gender: @admission.gender, mobile: @admission.mobile, mother_designation: @admission.mother_designation, mother_name: @admission.mother_name, name: @admission.name, school: @admission.school }
    assert_redirected_to admission_path(assigns(:admission))
  end

  test "should destroy admission" do
    assert_difference('Admission.count', -1) do
      delete :destroy, id: @admission
    end

    assert_redirected_to admissions_path
  end
end
