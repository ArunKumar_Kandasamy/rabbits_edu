# == Schema Information
#
# Table name: exam_result_subjects
#
#  id             :integer          not null, primary key
#  exam_result_id :integer
#  subject_id     :integer
#  grade          :string
#  total_marks    :integer
#  mark_obtained  :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'test_helper'

class ExamResultSubjectsControllerTest < ActionController::TestCase
  setup do
    @exam_result_subject = exam_result_subjects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:exam_result_subjects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create exam_result_subject" do
    assert_difference('ExamResultSubject.count') do
      post :create, exam_result_subject: { exam_result_id: @exam_result_subject.exam_result_id, grade: @exam_result_subject.grade, mark_obtained: @exam_result_subject.mark_obtained, subject_id: @exam_result_subject.subject_id, total_marks: @exam_result_subject.total_marks }
    end

    assert_redirected_to exam_result_subject_path(assigns(:exam_result_subject))
  end

  test "should show exam_result_subject" do
    get :show, id: @exam_result_subject
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @exam_result_subject
    assert_response :success
  end

  test "should update exam_result_subject" do
    patch :update, id: @exam_result_subject, exam_result_subject: { exam_result_id: @exam_result_subject.exam_result_id, grade: @exam_result_subject.grade, mark_obtained: @exam_result_subject.mark_obtained, subject_id: @exam_result_subject.subject_id, total_marks: @exam_result_subject.total_marks }
    assert_redirected_to exam_result_subject_path(assigns(:exam_result_subject))
  end

  test "should destroy exam_result_subject" do
    assert_difference('ExamResultSubject.count', -1) do
      delete :destroy, id: @exam_result_subject
    end

    assert_redirected_to exam_result_subjects_path
  end
end
