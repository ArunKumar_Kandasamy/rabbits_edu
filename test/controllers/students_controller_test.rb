# == Schema Information
#
# Table name: students
#
#  id                 :integer          not null, primary key
#  name               :string
#  roll_number        :string
#  gender             :string
#  father_name        :string
#  mother_name        :string
#  father_designation :string
#  mother_designation :string
#  address            :text
#  phone_number       :string
#  email              :string
#  status             :boolean
#  academic_level_id  :integer
#  first_language_id  :integer
#  second_language_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'test_helper'

class StudentsControllerTest < ActionController::TestCase
  setup do
    @student = students(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:students)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create student" do
    assert_difference('Student.count') do
      post :create, student: { address: @student.address, class_id: @student.class_id, email: @student.email, father_designation: @student.father_designation, father_name: @student.father_name, first_language_id: @student.first_language_id, gender: @student.gender, mother_designation: @student.mother_designation, mother_name: @student.mother_name, name: @student.name, phone_number: @student.phone_number, roll_number: @student.roll_number, second_language_id: @student.second_language_id, status: @student.status }
    end

    assert_redirected_to student_path(assigns(:student))
  end

  test "should show student" do
    get :show, id: @student
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @student
    assert_response :success
  end

  test "should update student" do
    patch :update, id: @student, student: { address: @student.address, class_id: @student.class_id, email: @student.email, father_designation: @student.father_designation, father_name: @student.father_name, first_language_id: @student.first_language_id, gender: @student.gender, mother_designation: @student.mother_designation, mother_name: @student.mother_name, name: @student.name, phone_number: @student.phone_number, roll_number: @student.roll_number, second_language_id: @student.second_language_id, status: @student.status }
    assert_redirected_to student_path(assigns(:student))
  end

  test "should destroy student" do
    assert_difference('Student.count', -1) do
      delete :destroy, id: @student
    end

    assert_redirected_to students_path
  end
end
