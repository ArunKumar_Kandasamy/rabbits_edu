# == Schema Information
#
# Table name: academic_levels_subjects
#
#  id                :integer          not null, primary key
#  academic_level_id :integer
#  subject_id        :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

require 'test_helper'

class AcademicLevelsSubjectsControllerTest < ActionController::TestCase
  setup do
    @academic_levels_subject = academic_levels_subjects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:academic_levels_subjects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create academic_levels_subject" do
    assert_difference('AcademicLevelsSubject.count') do
      post :create, academic_levels_subject: { academic_level_id: @academic_levels_subject.academic_level_id, subject_id: @academic_levels_subject.subject_id }
    end

    assert_redirected_to academic_levels_subject_path(assigns(:academic_levels_subject))
  end

  test "should show academic_levels_subject" do
    get :show, id: @academic_levels_subject
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @academic_levels_subject
    assert_response :success
  end

  test "should update academic_levels_subject" do
    patch :update, id: @academic_levels_subject, academic_levels_subject: { academic_level_id: @academic_levels_subject.academic_level_id, subject_id: @academic_levels_subject.subject_id }
    assert_redirected_to academic_levels_subject_path(assigns(:academic_levels_subject))
  end

  test "should destroy academic_levels_subject" do
    assert_difference('AcademicLevelsSubject.count', -1) do
      delete :destroy, id: @academic_levels_subject
    end

    assert_redirected_to academic_levels_subjects_path
  end
end
