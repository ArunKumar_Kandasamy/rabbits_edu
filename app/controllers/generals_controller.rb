# == Schema Information
#
# Table name: generals
#
#  id                   :integer          not null, primary key
#  logo_file_name       :string
#  logo_content_type    :string
#  logo_file_size       :integer
#  logo_updated_at      :datetime
#  favicon_file_name    :string
#  favicon_content_type :string
#  favicon_file_size    :integer
#  favicon_updated_at   :datetime
#  page_title           :string
#  meta_description     :text
#  analytics_text       :text
#  phone_number         :string
#  fb_link              :string
#  twitter_link         :string
#  address              :text
#  email                :string
#  home_page_content    :text
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class GeneralsController < ApplicationController
  before_action :set_general, only: [:show, :edit, :update, :destroy]

  # GET /generals
  # GET /generals.json
  def index
    @general = General.first
  end

  # GET /generals/1
  # GET /generals/1.json
  def show
  end

  # GET /generals/new
  def new
    @general = General.new
  end

  # GET /generals/1/edit
  def edit
    if params[:page_title].present?
      render :template => 'generals/edit_meta_tags'
    elsif params[:logo].present?
      render :template => 'generals/edit_logo'
    elsif params[:favicon].present?
      render :template => 'generals/edit_favicon'
    elsif params[:default_info].present?
      render :template => 'generals/edit_default_info'
    elsif params[:home_page_code].present?
      render :template => 'generals/edit_home_page_info'
    else
      render :template => 'generals/edit'
    end
  end

  # POST /generals
  # POST /generals.json
  def create
    @general = General.new(general_params)

    respond_to do |format|
      if @general.save
        format.html { redirect_to @general, notice: 'General was successfully created.' }
        format.json { render :show, status: :created, location: @general }
      else
        format.html { render :new }
        format.json { render json: @general.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /generals/1
  # PATCH/PUT /generals/1.json
  def update
    respond_to do |format|
      if @general.update(general_params)
        format.html { redirect_to generals_path, notice: 'General was successfully updated.' }
        format.json { render :show, status: :ok, location: @general }
      else
        if params[:general][:page_title].present? || params[:general][:meta_description].present?
          format.html { render :template => 'generals/edit_meta_tags' }
        elsif params[:general][:logo].present?
          format.html { render :template => 'generals/edit_logo' }
        elsif params[:general][:favicon].present?
          format.html { render :template => 'generals/edit_favicon' }
        elsif params[:general][:default_info].present?
          format.html { render :template => 'generals/edit_default_info' }
        elsif params[:home_page_code].present?
          format.html { render :template => 'generals/edit_home_page_info' }
        else
          render :template => 'generals/edit'
        end
        format.json { render json: @general.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /generals/1
  # DELETE /generals/1.json
  def destroy
    @general.destroy
    respond_to do |format|
      format.html { redirect_to generals_url, notice: 'General was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_general
      @general = General.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def general_params
      params.require(:general).permit(:logo, :favicon, :page_title, :meta_description, :analytics_text, :phone_number, :fb_link, :twitter_link, :address, :email, :home_page_content)
    end
end
