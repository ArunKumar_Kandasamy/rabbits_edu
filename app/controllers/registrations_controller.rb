class RegistrationsController < Devise::RegistrationsController
  def create
    if params[:user][:roll_number].present?
      @student = Student.find_by_roll_number_and_email(params[:user][:roll_number],params[:user][:email])
      if @student.present?
        @user= User.new(user_params)
        if @user.save
          respond_to do |format|
            format.html{
              sign_up(resource_name, resource)
              redirect_to students_dashboard_path, notice: "Account Successfully Created"
            }
          end
        else
          respond_to do |format|
            format.html{
              redirect_to new_user_registration_path, notice: "Invalid roll number or email"
            }
          end
        end
      else
        respond_to do |format|
          format.html{
            redirect_to new_user_registration_path, notice: "Invalid roll number or email"
          }
        end
      end
    end
  end

  def update
  	super
  end

  protected


    def after_update_path_for(resource)
      '/students_dashboard'
    end

    def after_inactive_sign_up_path_for(resource)
      '/users/sign_in' # Or :prefix_to_your_route
    end


    def after_sign_in_path_for(resource)
      '/students_dashboard' # Or :prefix_to_your_route
    end


  private
    def set_user
      @user =User.find(params[:id])
    end

      # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:roll_number, :email, :password, :password_confirmation, :remember_me, :current_password)
    end

end
