# == Schema Information
#
# Table name: grades
#
#  id         :integer          not null, primary key
#  range      :string
#  grade      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class StudentNotificationsController < ApplicationController
  before_action :set_notification_type, only: [:show, :edit, :update, :destroy]
  before_action :set_user

  def dashboard
    @recent_notifications = DynamicContent.all.order(:created_at).limit(6)
  end

  def index
    @notification_types = NotificationType.all
  end

  def show
    @notifications = @notification_type.dynamic_contents.where(:status => true)
  end

  def showcase
    @notification_content = DynamicContent.find_by_slug(params[:student_notification_id])
    @notification = @notification_content.notification_type
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notification_type
      @notification_type = NotificationType.find_by_slug(params[:id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = current_user
    end
end
