# == Schema Information
#
# Table name: academic_levels_subjects
#
#  id                :integer          not null, primary key
#  academic_level_id :integer
#  subject_id        :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class AcademicLevelsSubjectsController < ApplicationController
  before_action :set_academic_levels_subject, only: [:show, :edit, :update, :destroy]

  # GET /academic_levels_subjects
  # GET /academic_levels_subjects.json
  def index
    @academic_levels_subjects = AcademicLevelsSubject.all
  end

  # GET /academic_levels_subjects/1
  # GET /academic_levels_subjects/1.json
  def show
  end

  # GET /academic_levels_subjects/new
  def new
    @academic_levels_subject = AcademicLevelsSubject.new
  end

  # GET /academic_levels_subjects/1/edit
  def edit
  end

  # POST /academic_levels_subjects
  # POST /academic_levels_subjects.json
  def create
    @academic_levels_subject = AcademicLevelsSubject.new(academic_levels_subject_params)

    respond_to do |format|
      if @academic_levels_subject.save
        format.html { redirect_to @academic_levels_subject, notice: 'Academic levels subject was successfully created.' }
        format.json { render :show, status: :created, location: @academic_levels_subject }
      else
        format.html { render :new }
        format.json { render json: @academic_levels_subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /academic_levels_subjects/1
  # PATCH/PUT /academic_levels_subjects/1.json
  def update
    respond_to do |format|
      if @academic_levels_subject.update(academic_levels_subject_params)
        format.html { redirect_to @academic_levels_subject, notice: 'Academic levels subject was successfully updated.' }
        format.json { render :show, status: :ok, location: @academic_levels_subject }
      else
        format.html { render :edit }
        format.json { render json: @academic_levels_subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /academic_levels_subjects/1
  # DELETE /academic_levels_subjects/1.json
  def destroy
    @academic_levels_subject.destroy
    respond_to do |format|
      format.html { redirect_to academic_levels_subjects_url, notice: 'Academic levels subject was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_academic_levels_subject
      @academic_levels_subject = AcademicLevelsSubject.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def academic_levels_subject_params
      params.require(:academic_levels_subject).permit(:academic_level_id, :subject_id)
    end
end
