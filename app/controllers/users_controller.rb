# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  roll_number            :string
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class UsersController < ApplicationController
    before_action :authenticate_admin!, only: [ :index]  
    before_action :set_user, only: [ :show, :destroy]

	  def index
	    @users = User.all
	  end

	  def destroy
	        @user.destroy
	        respond_to do |format|
	          format.html { redirect_to users_path, notice: 'User was successfully destroyed.' }
	          format.json { head :no_content }
	        end
	  end

      private
          # Use callbacks to share common setup or constraints between actions.
          def set_user
              if current_user.present?
                  @user = current_user
              else
                  redirect_to login_url
              end
          end

end
