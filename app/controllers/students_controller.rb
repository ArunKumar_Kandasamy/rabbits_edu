# == Schema Information
#
# Table name: students
#
#  id                 :integer          not null, primary key
#  name               :string
#  roll_number        :string
#  gender             :string
#  father_name        :string
#  mother_name        :string
#  father_designation :string
#  mother_designation :string
#  address            :text
#  phone_number       :string
#  email              :string
#  status             :boolean
#  academic_level_id  :integer
#  first_language_id  :integer
#  second_language_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy]

  # GET /students
  # GET /students.json
  def index
    @students = Student.all
  end

  # GET /students/1
  # GET /students/1.json
  def show
  end

  def import_students
  end

  def import
    Student.import(params[:file])
    redirect_to students_path, notice: "Students imported."
  end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit(:dob, :name, :roll_number, :gender, :father_name, :mother_name, :father_designation, :mother_designation, :address, :phone_number, :email, :status, :academic_level_id, :first_language_id, :second_language_id, :invitation_sent , :invitation_status)
    end
end
