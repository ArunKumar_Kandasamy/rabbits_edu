# == Schema Information
#
# Table name: dynamic_contents
#
#  id                   :integer          not null, primary key
#  title                :string
#  description          :string
#  slug                 :string
#  status               :boolean
#  notification_type_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class DynamicContentsController < ApplicationController
  before_action :set_dynamic_content, only: [:show, :edit, :update, :destroy]

  # GET /dynamic_contents
  # GET /dynamic_contents.json
  def index
    @dynamic_contents = DynamicContent.all
  end

  # GET /dynamic_contents/1
  # GET /dynamic_contents/1.json
  def show
  end

  # GET /dynamic_contents/new
  def new
    @dynamic_content = DynamicContent.new
  end

  # GET /dynamic_contents/1/edit
  def edit
  end

  # POST /dynamic_contents
  # POST /dynamic_contents.json
  def create
    @dynamic_content = DynamicContent.new(dynamic_content_params)

    respond_to do |format|
      if @dynamic_content.save
        format.html { redirect_to @dynamic_content, notice: 'Dynamic content was successfully created.' }
        format.json { render :show, status: :created, location: @dynamic_content }
      else
        format.html { render :new }
        format.json { render json: @dynamic_content.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dynamic_contents/1
  # PATCH/PUT /dynamic_contents/1.json
  def update
    respond_to do |format|
      if @dynamic_content.update(dynamic_content_params)
        format.html { redirect_to @dynamic_content, notice: 'Dynamic content was successfully updated.' }
        format.json { render :show, status: :ok, location: @dynamic_content }
      else
        format.html { render :edit }
        format.json { render json: @dynamic_content.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dynamic_contents/1
  # DELETE /dynamic_contents/1.json
  def destroy
    @dynamic_content.destroy
    respond_to do |format|
      format.html { redirect_to dynamic_contents_url, notice: 'Dynamic content was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dynamic_content
      @dynamic_content = DynamicContent.find_by_slug(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dynamic_content_params
      params.require(:dynamic_content).permit(:title, :description, :slug, :status, :notification_type_id)
    end
end
