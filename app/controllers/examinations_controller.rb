# == Schema Information
#
# Table name: examinations
#
#  id         :integer          not null, primary key
#  exam_no    :string
#  exam_name  :string
#  mark_type  :string
#  visibility :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ExaminationsController < ApplicationController
  before_action :set_examination, only: [:show, :edit, :update, :destroy]

  # GET /examinations
  # GET /examinations.json
  def index
    @examinations = Examination.all
  end

  # GET /examinations/1
  # GET /examinations/1.json
  def show
    @exam_results = @examination.exam_results
  end

  # GET /examinations/new
  def new
    @examination = Examination.new
  end

  # GET /examinations/1/edit
  def edit
  end

  # POST /examinations
  # POST /examinations.json
  def create
    @examination = Examination.new(examination_params)
    respond_to do |format|
      if @examination.save
        format.html { redirect_to @examination, notice: 'Examination was successfully created.' }
        format.json { render :show, status: :created, location: @examination }
      else
        format.html { render :new }
        format.json { render json: @examination.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /examinations/1
  # PATCH/PUT /examinations/1.json
  def update
    respond_to do |format|
      if @examination.update(examination_params)
        format.html { redirect_to @examination, notice: 'Examination was successfully updated.' }
        format.json { render :show, status: :ok, location: @examination }
      else
        format.html { render :edit }
        format.json { render json: @examination.errors, status: :unprocessable_entity }
      end
    end
  end

  def find_exam_results
    if params[:roll_number].present? && params[:dob].present?
      @student = Student.where("roll_number = ? AND dob = ?",params[:roll_number],params[:dob])
      if @student.present?
        @examination = Examination.find_by_exam_no(params[:exam_result][:exam_no])
        if @examination.present?
          @exam_results = @examination.exam_results.where(:student_id => @student.first.id)
          render "static_pages/find_exam_result.js.erb"
        else
          render "static_pages/exam_result_status.js.erb"
        end
      else
        render "static_pages/exam_result_status.js.erb"  
      end 
    else
      render "static_pages/exam_result_status.js.erb" 
    end
  end

  def import_exam_results
    @examination = Examination.find(params[:examination_id])
  end

  # DELETE /examinations/1
  # DELETE /examinations/1.json
  def destroy
    @examination.destroy
    respond_to do |format|
      format.html { redirect_to examinations_url, notice: 'Examination was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_examination
      @examination = Examination.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def examination_params
      params.require(:examination).permit(:exam_no, :exam_name, :mark_type, :visibility)
    end
end
