# == Schema Information
#
# Table name: exam_result_subjects
#
#  id             :integer          not null, primary key
#  exam_result_id :integer
#  subject_id     :integer
#  grade          :string
#  total_marks    :integer
#  mark_obtained  :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class ExamResultSubjectsController < ApplicationController
  before_action :set_exam_result_subject, only: [:show, :edit, :update, :destroy]
  before_action :set_examination
  before_action :set_exam_result

  # GET /exam_result_subjects
  # GET /exam_result_subjects.json
  def index
    @exam_result_subjects = ExamResultSubject.all
  end

  # GET /exam_result_subjects/1
  # GET /exam_result_subjects/1.json
  def show
  end

  def import
    ExamResultSubject.import(params[:file],params[:examination_id])
    redirect_to examinations_path, notice: "Examination Results imported successfully."
  end

  # GET /exam_result_subjects/new
  def new
    @exam_result_subject = ExamResultSubject.new
  end

  # GET /exam_result_subjects/1/edit
  def edit
  end

  # POST /exam_result_subjects
  # POST /exam_result_subjects.json
  def create
    @exam_result_subject = ExamResultSubject.new(exam_result_subject_params)

    respond_to do |format|
      if @exam_result_subject.save
        format.html { redirect_to examination_exam_result_exam_result_subjects_path(@examination,@exam_result), notice: 'Exam result subject was successfully created.' }
        format.json { render :show, status: :created, location: @exam_result_subject }
      else
        format.html { render :new }
        format.json { render json: @exam_result_subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /exam_result_subjects/1
  # PATCH/PUT /exam_result_subjects/1.json
  def update
    respond_to do |format|
      if @exam_result_subject.update(exam_result_subject_params)
        format.html { redirect_to examination_exam_result_exam_result_subjects_path(@examination,@exam_result,@exam_result_subject), notice: 'Exam result subject was successfully updated.' }
        format.json { render :show, status: :ok, location: @exam_result_subject }
      else
        format.html { render :edit }
        format.json { render json: @exam_result_subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /exam_result_subjects/1
  # DELETE /exam_result_subjects/1.json
  def destroy
    @exam_result_subject.destroy
    respond_to do |format|
      format.html { redirect_to exam_result_subjects_url, notice: 'Exam result subject was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_examination
      @examination = Examination.find(params[:examination_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_exam_result
      @exam_result = ExamResult.find(params[:exam_result_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_exam_result_subject
      @exam_result_subject = ExamResultSubject.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def exam_result_subject_params
      params.require(:exam_result_subject).permit(:exam_result_id, :subject_id, :grade, :total_marks, :mark_obtained)
    end
end
