json.array!(@exam_result_subjects) do |exam_result_subject|
  json.extract! exam_result_subject, :id, :exam_result_id, :subject_id, :grade, :total_marks, :mark_obtained
  json.url exam_result_subject_url(exam_result_subject, format: :json)
end
