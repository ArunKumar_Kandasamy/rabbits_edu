json.array!(@exam_results) do |exam_result|
  json.extract! exam_result, :id, :examination_id, :student_id, :total_score, :result_status, :published_at
  json.url exam_result_url(exam_result, format: :json)
end
