json.array!(@examinations) do |examination|
  json.extract! examination, :id, :exam_no, :exam_name, :mark_type, :visibility
  json.url examination_url(examination, format: :json)
end
