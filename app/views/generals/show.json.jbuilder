json.extract! @general, :id, :logo, :favicon, :page_title, :meta_description, :analytics_text, :phone_number, :fb_link, :twitter_link, :address, :email, :created_at, :updated_at
