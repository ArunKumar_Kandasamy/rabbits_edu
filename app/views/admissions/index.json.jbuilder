json.array!(@admissions) do |admission|
  json.extract! admission, :id, :name, :gender, :dob, :class, :school, :father_name, :father_designation, :mother_name, :mother_designation, :address, :mobile, :email, :annual_income
  json.url admission_url(admission, format: :json)
end
