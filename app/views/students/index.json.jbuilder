json.array!(@students) do |student|
  json.extract! student, :id, :name, :roll_number, :gender, :father_name, :mother_name, :father_designation, :mother_designation, :address, :phone_number, :email, :status, :class_id, :first_language_id, :second_language_id
  json.url student_url(student, format: :json)
end
