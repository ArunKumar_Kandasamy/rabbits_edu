json.array!(@academic_levels) do |academic_level|
  json.extract! academic_level, :id, :name
  json.url academic_level_url(academic_level, format: :json)
end
