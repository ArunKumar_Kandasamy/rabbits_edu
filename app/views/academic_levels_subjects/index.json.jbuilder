json.array!(@academic_levels_subjects) do |academic_levels_subject|
  json.extract! academic_levels_subject, :id, :academic_level_id, :subject_id
  json.url academic_levels_subject_url(academic_levels_subject, format: :json)
end
