# == Schema Information
#
# Table name: exam_result_subjects
#
#  id             :integer          not null, primary key
#  exam_result_id :integer
#  subject_id     :integer
#  grade          :string
#  total_marks    :integer
#  mark_obtained  :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class ExamResultSubject < ActiveRecord::Base
    belongs_to :exam_result, class_name:"ExamResult"
    belongs_to :subject, class_name:"Subject"
    after_save :update_total

	def update_total
		@exam_result = self.exam_result
		@total = 0
		@exam_result.exam_result_subjects.each do |exam_result_subject|
			@total += exam_result_subject.mark_obtained
		end
		@exam_result.total_score = @total
		@exam_result.save
	end

    def self.accessible_attributes
        ["roll_number", "name", "gender", "father_name", "mother_name", "father_designation", "mother_designation", "phone_number", "address", "email_id", "status"]
    end

    def self.import(file, examination_id)
        spreadsheet = open_spreadsheet(file)
        header = spreadsheet.row(1)
        (2..spreadsheet.last_row).each do |i|
          row = Hash[[header, spreadsheet.row(i)].transpose]
	        @student = Student.find_by_roll_number(row["roll_number"].to_s)
	        exam_results = ExamResult.where("student_id = ? AND examination_id = ?",@student.id , examination_id)
	        if exam_results.present?
	           	exam_result = exam_results.first
	        else
	           	exam_result = ExamResult.new
	        end
	       exam_result.examination_id = examination_id
           exam_result.student_id = @student.id
           exam_result.save!
           exam_result_subject = find_by_subject_id_and_exam_result_id(row["subject_id"].to_s , exam_result.id) || new
           exam_result_subject.exam_result_id = exam_result.id
           exam_result_subject.subject_id = row["subject_id"].to_i
           exam_result_subject.grade = row["grade"].to_s
           exam_result_subject.total_marks = row["total_marks"].to_s
           exam_result_subject.mark_obtained = row["mark_obtained"].to_s
           exam_result_subject.save!
        end
    end

    def self.open_spreadsheet(file)
          case File.extname(file.original_filename)
          when '.csv' then Roo::CSV.new(file.path)
          when '.xls' then Roo::Excel.new(file.path)
          when '.xlsx' then excel_import(file)
          else raise "Unknown file type: #{file.original_filename}"
          end
    end
      

    def self.excel_import(file)
        tmp = file.tempfile
        tmp_file = File.join("public", file.original_filename)
        FileUtils.cp tmp.path, tmp_file
        import = Roo::Excelx.new(tmp_file)
    end
end
