# == Schema Information
#
# Table name: students
#
#  id                 :integer          not null, primary key
#  name               :string
#  roll_number        :string
#  gender             :string
#  father_name        :string
#  mother_name        :string
#  father_designation :string
#  mother_designation :string
#  address            :text
#  phone_number       :string
#  email              :string
#  status             :boolean
#  academic_level_id  :integer
#  first_language_id  :integer
#  second_language_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Student < ActiveRecord::Base
    belongs_to :academic_level, class_name:"AcademicLevel"
    before_save :update_invitaion_status

      def update_invitaion_status
        if self.invitation_sent != true && self.invitation_status == true
          StudentMailer.send_invitation(self).deliver
          self.invitation_sent = true
        end 
      end


      def self.accessible_attributes
          ["roll_number", "name", "gender", "father_name", "mother_name", "father_designation", "mother_designation", "phone_number", "address", "email_id", "status"]
      end

      def self.import(file)
        spreadsheet = open_spreadsheet(file)
        header = spreadsheet.row(1)
        (2..spreadsheet.last_row).each do |i|
          row = Hash[[header, spreadsheet.row(i)].transpose]
           student = find_by_roll_number(row["roll_number"].to_s) || new
           student.roll_number = row["roll_number"].to_s
           student.name = row["name"].to_s
           student.gender = row["gender"].to_s
           student.father_name = row["father_name"].to_s
           student.mother_name = row["mother_name"].to_s
           student.father_designation = row["father_designation"].to_s
           student.mother_designation = row["mother_designation"].to_s
           student.address = row["address"].to_s
           student.phone_number = row["phone_number"].to_i.to_s
           student.email = row["email_id"].to_s
           student.invitation_status = row["invitation_status"].to_i.to_s
           student.status = row["status"].to_i.to_s
           student.save!
        end
      end

      def self.open_spreadsheet(file)
          case File.extname(file.original_filename)
          when '.csv' then Roo::CSV.new(file.path)
          when '.xls' then Roo::Excel.new(file.path)
          when '.xlsx' then excel_import(file)
          else raise "Unknown file type: #{file.original_filename}"
          end
      end
      

      def self.excel_import(file)
        tmp = file.tempfile
        tmp_file = File.join("public", file.original_filename)
        FileUtils.cp tmp.path, tmp_file
        import = Roo::Excelx.new(tmp_file)
      end

end
