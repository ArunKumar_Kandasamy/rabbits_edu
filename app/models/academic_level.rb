# == Schema Information
#
# Table name: academic_levels
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AcademicLevel < ActiveRecord::Base
    has_many :academic_levels_subjects, class_name:"AcademicLevelsSubject"
	has_many :subjects, class_name:"Subject", through: :subjects
	has_many :students, class_name:"Student"
end
