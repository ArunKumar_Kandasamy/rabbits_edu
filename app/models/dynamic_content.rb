# == Schema Information
#
# Table name: dynamic_contents
#
#  id                   :integer          not null, primary key
#  title                :string
#  description          :string
#  slug                 :string
#  status               :boolean
#  notification_type_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class DynamicContent < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  belongs_to :notification_type, class_name:"NotificationType"
end
