# == Schema Information
#
# Table name: subjects
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Subject < ActiveRecord::Base
    has_many :academic_levels_subjects, class_name:"AcademicLevelsSubject"
	has_many :academic_levels, class_name:"AcademicLevel", through: :academic_levels_subjects
end
