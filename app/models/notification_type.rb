# == Schema Information
#
# Table name: notification_types
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class NotificationType < ActiveRecord::Base
    extend FriendlyId
    friendly_id :name, use: :slugged
	has_many :dynamic_contents, class_name:"DynamicContent"

    def should_generate_new_friendly_id?
      name_changed?
    end

end
