# == Schema Information
#
# Table name: generals
#
#  id                   :integer          not null, primary key
#  logo_file_name       :string
#  logo_content_type    :string
#  logo_file_size       :integer
#  logo_updated_at      :datetime
#  favicon_file_name    :string
#  favicon_content_type :string
#  favicon_file_size    :integer
#  favicon_updated_at   :datetime
#  page_title           :string
#  meta_description     :text
#  analytics_text       :text
#  phone_number         :string
#  fb_link              :string
#  twitter_link         :string
#  address              :text
#  email                :string
#  home_page_content    :text
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class General < ActiveRecord::Base
  has_attached_file :logo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  has_attached_file :favicon, styles:{ thumb: "16x16!" }
  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\Z/
  validates_attachment_content_type :favicon, content_type: [ "image/x-icon","image/vnd.microsoft.icon" ]
end
