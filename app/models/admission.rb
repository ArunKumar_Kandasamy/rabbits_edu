class Admission < ActiveRecord::Base
	before_create :create_admission_number

	def create_admission_number
		datee = Date.today.to_datetime
	    day = datee.mday
	    daily = day.to_s.size
	    if daily == 1
	      day = "0#{day}"
	    else
	      day = day
	    end
	    month = datee.mon
        admission_data = rand(5..90)
		self.admission_number = "SVI#{day}#{month}000#{admission_data}"
	end
end
