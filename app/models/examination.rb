# == Schema Information
#
# Table name: examinations
#
#  id         :integer          not null, primary key
#  exam_no    :string
#  exam_name  :string
#  mark_type  :string
#  visibility :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Examination < ActiveRecord::Base
	has_many :exam_results, class_name:"ExamResult"
end
