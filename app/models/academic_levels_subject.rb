# == Schema Information
#
# Table name: academic_levels_subjects
#
#  id                :integer          not null, primary key
#  academic_level_id :integer
#  subject_id        :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class AcademicLevelsSubject < ActiveRecord::Base
    belongs_to :academic_levels, class_name:"AcademicLevel"
    belongs_to :subject, class_name:"Subject"
end
