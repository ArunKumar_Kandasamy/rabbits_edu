module ApplicationHelper

	def pending_notification
	   @pending_notifications = NotificationType.order(:created_at).limit(6)
	end

	def pending_notification_count
		@flag_count = 0
		pending_notification.each do |pending_notification|
          if pending_notification.dynamic_contents.count > 0
          	@pending_notify = pending_notification.dynamic_contents.where("created_at >= ?", @current_user.notification_view).where(:status => true)
          	if @pending_notify.present?
          	  @flag_count += 1
          	end
          end
		end
		return @flag_count
	end

	def pending_notification_symbol(pending_notification)
		@pending_notification_slug = pending_notification.slug
		if @pending_notification_slug == "latest-happenings"
			return "fa-bell text-info"
		elsif @pending_notification_slug == "upcoming-events"
			return "fa-envelope text-success"
		elsif @pending_notification_slug == "holidays"
			return "fa-plane text-danger"
		elsif @pending_notification_slug == "notices"
			return "fa-flag text-warning"
		end
	end

	def general
		@general = General.first
	end

	def update_notification_view
	    @current_user.notification_view = Time.now
	    @current_user.save
	end

	def notification_types
		@notification_types = NotificationType.all
	end

	def notice_color_toggle(flag)
	  @flag_mod = @flag % 3
      if @flag_mod == 0 
      	return 'yellow' 
      elsif @flag_mod == 1
      	return 'blue'
      elsif @flag_mod == 2
      	return 'green'
      end
	end

	def notification_content_count(notification_type) 
		@notify_content = notification_type.dynamic_contents.where(:status => true)
		if @notify_content.present?
			return @notify_content.count
		else
			return 0
		end
	end
	
	def find_user_role
		if controller_name == "admissions" || controller_name == "exam_result_subjects" || controller_name == "exam_results" || controller_name == "examinations" || controller_name == "students" || controller_name == "dashboards" || controller_name == "subjects"  || controller_name == "grades" || controller_name == "academic_levels" || controller_name == "notification_types" || controller_name == "dynamic_contents" || controller_name == "generals" || (controller_name == "users" && action_name == "index") || controller_name == "admins"
	        return "admin"
	    else
	    	return "student"
	    end
	end
end
