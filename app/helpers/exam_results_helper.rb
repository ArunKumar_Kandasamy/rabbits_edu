# == Schema Information
#
# Table name: exam_results
#
#  id             :integer          not null, primary key
#  examination_id :integer
#  student_id     :integer
#  total_score    :integer
#  result_status  :string
#  published_at   :datetime
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

module ExamResultsHelper
end
