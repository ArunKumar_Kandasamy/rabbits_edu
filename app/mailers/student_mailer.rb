class StudentMailer < ActionMailer::Base

  default from: "preethi.m@coderabbits.com"

  def send_invitation(student)
    @student = student
    mail(
      :subject => "Invitation",
      :to  => @student.email)
  end
end