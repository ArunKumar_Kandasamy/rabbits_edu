class AddAdmissionNumberToAdmissions < ActiveRecord::Migration
  def change
  	add_column :admissions, :admission_number, :string
  end
end
