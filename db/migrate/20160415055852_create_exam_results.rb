class CreateExamResults < ActiveRecord::Migration
  def change
    create_table :exam_results do |t|
      t.integer :examination_id
      t.integer :student_id
      t.integer :total_score
      t.string :result_status
      t.datetime :published_at

      t.timestamps null: false
    end
  end
end
