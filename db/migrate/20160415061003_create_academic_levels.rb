class CreateAcademicLevels < ActiveRecord::Migration
  def change
    create_table :academic_levels do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
