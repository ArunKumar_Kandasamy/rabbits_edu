class AddSlugToNotificationTypes < ActiveRecord::Migration
  def change
    add_column :notification_types, :slug, :string
    add_index :notification_types, :slug
  end
end
