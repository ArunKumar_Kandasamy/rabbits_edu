class AddPreviousSchoolToAdmissions < ActiveRecord::Migration
  def change
  	add_column :admissions, :previous_school, :string
  end
end
