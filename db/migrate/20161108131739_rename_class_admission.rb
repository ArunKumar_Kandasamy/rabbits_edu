class RenameClassAdmission < ActiveRecord::Migration
  def change
  	remove_column :admissions, :class
  	add_column :admissions, :academic_level, :string
  end
end
