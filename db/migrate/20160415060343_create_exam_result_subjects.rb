class CreateExamResultSubjects < ActiveRecord::Migration
  def change
    create_table :exam_result_subjects do |t|
      t.integer :exam_result_id
      t.integer :subject_id
      t.string :grade
      t.integer :total_marks
      t.integer :mark_obtained

      t.timestamps null: false
    end
  end
end
