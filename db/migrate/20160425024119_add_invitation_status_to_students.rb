class AddInvitationStatusToStudents < ActiveRecord::Migration
  def change
    add_column :students, :invitation_status, :boolean, default: false
    add_column :students, :invitation_sent, :boolean, default: false
  end
end
