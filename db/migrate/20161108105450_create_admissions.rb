class CreateAdmissions < ActiveRecord::Migration
  def change
    create_table :admissions do |t|
      t.string :name
      t.string :gender
      t.date :dob
      t.string :class
      t.string :school
      t.string :father_name
      t.string :father_designation
      t.string :mother_name
      t.string :mother_designation
      t.string :address
      t.string :mobile
      t.string :email
      t.float :annual_income

      t.timestamps null: false
    end
  end
end
