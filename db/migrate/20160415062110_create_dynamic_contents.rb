class CreateDynamicContents < ActiveRecord::Migration
  def change
    create_table :dynamic_contents do |t|
      t.string :title
      t.string :description
      t.string :slug
      t.boolean :status
      t.integer :notification_type_id

      t.timestamps null: false
    end
  end
end
