class AddNotificationViewToUsers < ActiveRecord::Migration
  def change
    remove_column :admins, :notification_view
    add_column :users, :notification_view, :timestamp
  end
end
