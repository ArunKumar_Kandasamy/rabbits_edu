class AddRollNumberToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :roll_number, :string
  end
end
