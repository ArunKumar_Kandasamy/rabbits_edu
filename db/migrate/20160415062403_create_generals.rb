class CreateGenerals < ActiveRecord::Migration
  def change
    create_table :generals do |t|
      t.attachment :logo
      t.attachment :favicon
      t.string :page_title
      t.text :meta_description
      t.text :analytics_text
      t.string :phone_number
      t.string :fb_link
      t.string :twitter_link
      t.text :address
      t.string :email
      t.text :home_page_content

      t.timestamps null: false
    end
  end
end
