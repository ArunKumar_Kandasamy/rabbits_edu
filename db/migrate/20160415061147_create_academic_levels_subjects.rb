class CreateAcademicLevelsSubjects < ActiveRecord::Migration
  def change
    create_table :academic_levels_subjects do |t|
      t.integer :academic_level_id
      t.integer :subject_id

      t.timestamps null: false
    end
  end
end
