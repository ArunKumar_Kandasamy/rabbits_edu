class AddNotificationViewToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :notification_view, :timestamp
  end
end
