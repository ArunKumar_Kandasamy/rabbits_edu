class CreateExaminations < ActiveRecord::Migration
  def change
    create_table :examinations do |t|
      t.string :exam_no
      t.string :exam_name
      t.string :mark_type
      t.boolean :visibility

      t.timestamps null: false
    end
  end
end
