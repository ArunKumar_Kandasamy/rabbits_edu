class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :name
      t.string :roll_number
      t.string :gender
      t.string :father_name
      t.string :mother_name
      t.string :father_designation
      t.string :mother_designation
      t.text :address
      t.string :phone_number
      t.string :email
      t.boolean :status
      t.integer :academic_level_id
      t.integer :first_language_id
      t.integer :second_language_id

      t.timestamps null: false
    end
  end
end
