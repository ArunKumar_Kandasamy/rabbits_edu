# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161109071823) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "academic_levels", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "academic_levels_subjects", force: :cascade do |t|
    t.integer  "academic_level_id"
    t.integer  "subject_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "roll_number"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "admissions", force: :cascade do |t|
    t.string   "name"
    t.string   "gender"
    t.date     "dob"
    t.string   "school"
    t.string   "father_name"
    t.string   "father_designation"
    t.string   "mother_name"
    t.string   "mother_designation"
    t.string   "address"
    t.string   "mobile"
    t.string   "email"
    t.float    "annual_income"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "academic_level"
    t.string   "admission_number"
    t.string   "previous_school"
  end

  create_table "dynamic_contents", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.string   "slug"
    t.boolean  "status"
    t.integer  "notification_type_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "exam_result_subjects", force: :cascade do |t|
    t.integer  "exam_result_id"
    t.integer  "subject_id"
    t.string   "grade"
    t.integer  "total_marks"
    t.integer  "mark_obtained"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "exam_results", force: :cascade do |t|
    t.integer  "examination_id"
    t.integer  "student_id"
    t.integer  "total_score"
    t.string   "result_status"
    t.datetime "published_at"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "examinations", force: :cascade do |t|
    t.string   "exam_no"
    t.string   "exam_name"
    t.string   "mark_type"
    t.boolean  "visibility"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "generals", force: :cascade do |t|
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "favicon_file_name"
    t.string   "favicon_content_type"
    t.integer  "favicon_file_size"
    t.datetime "favicon_updated_at"
    t.string   "page_title"
    t.text     "meta_description"
    t.text     "analytics_text"
    t.string   "phone_number"
    t.string   "fb_link"
    t.string   "twitter_link"
    t.text     "address"
    t.string   "email"
    t.text     "home_page_content"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "grades", force: :cascade do |t|
    t.string   "range"
    t.string   "grade"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notification_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
  end

  add_index "notification_types", ["slug"], name: "index_notification_types_on_slug", using: :btree

  create_table "students", force: :cascade do |t|
    t.string   "name"
    t.string   "roll_number"
    t.string   "gender"
    t.string   "father_name"
    t.string   "mother_name"
    t.string   "father_designation"
    t.string   "mother_designation"
    t.text     "address"
    t.string   "phone_number"
    t.string   "email"
    t.boolean  "status"
    t.integer  "academic_level_id"
    t.integer  "first_language_id"
    t.integer  "second_language_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.date     "dob"
    t.boolean  "invitation_status",  default: false
    t.boolean  "invitation_sent",    default: false
  end

  create_table "subjects", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "roll_number"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.datetime "notification_view"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["roll_number"], name: "index_users_on_roll_number", unique: true, using: :btree

end
